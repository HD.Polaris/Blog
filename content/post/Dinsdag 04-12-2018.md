---
title: Dinsdag 04 december
date: 2018-12-04
---
Vandaag heb ik mijn schermen interactief gemaakt tot een low-fid prototype. Ik heb het zo gemaakt dat de alternatieve routes ook aan te pas komen. Ook heb ik de stijl veranderd om meer een eenheid te creëren met de huidige website van Woonstad.
