---
title: Eindreflectie kwartaal 1
date: 2018-10-22
---
Wat ik door het project heen geleerd heb is dat samenwerken heel belangrijk is binnen het team en dat je alleen als team tot de beste producten komt (2 weten er meer dan 1), dit heb ik ook nog verder geleerd bij het lab Creative Concepting. Door samen te werken realiseer je een concept waar niet alleen jij maar heel het team het mee eens is waardoor het team meer gemotiveerd is om door te werken. Verder vond ik de algemene werksfeer erg prettig en vind ik dat het project goed is verlopen.
