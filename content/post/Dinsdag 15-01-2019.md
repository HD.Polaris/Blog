---
title: Dinsdag 15 januari
date: 2019-01-15
---
Vandaag zijn we bezig geweest met het maken van ons ontwerpverslag. Hiervoor heb ik de waardenpropositie opnieuw gemaakt om aan het nieuwe concept te koppelen, daarbij heb ik ook een duidelijke introductie geschreven net zoals voor het Business Model Canvas. We hebben samen ook de ontwerpproceskaart samengesteld om ons proces in kaart te brengen en hierbij een toelichting geformuleerd. Later zijn we begonnen met het voorbereiden op de proef expo.
