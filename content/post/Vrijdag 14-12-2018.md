---
title: Vrijdag 14 december
date: 2018-12-14
---
Vandaag heb ik een waardenpropositie gemaakt en een business model canvas. Hiervoor ben ik gaan kijken naar de waarden en behoeftes van zowel de klant als de gebruiker en deze in verschillende categorieën geformuleerd. Voor het business model canvas heb ik elk hoofd en sub onderdeel onderzocht en de informatie van ons huidige prototype op toegepast.
