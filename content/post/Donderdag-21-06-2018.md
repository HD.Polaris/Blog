---
title: Donderdag 21 juni
date: 2018-06-21
---
Vandaag zijn we begonnen met het presenteren van onze onderzoek posters, daarbij hebben we feedback gegeven en ontvangen op de inhoud en de vorm door middel van de roddel techniek. Na de feedback heb ik een transfer geschreven en dit meegenomen voor de volgende keer. Daarna zijn we begonnen met een creatieve sessie. Hiervoor heb ik 2 divergerende technieken gezocht namelijk: woordspin en de braindump. Nadat ik meerdere ideeën had gegenereerd was het tijd om te convergeren door middel van de stickermethode. Uiteindelijk is hier een eindconcept uit ontstaan en heb ik er een korte beschrijving bij gemaakt en een ondersteunend moodboard. Later heb ik alle conceptinformatie verwerkt in een concept poster om zo mijn concept in tekst en beeld te laten zien.
