---
title: Donderdag 28 juni
date: 2018-06-28
---
Vandaag ben ik op school verder gegaan met het maken van mijn high-fid prototype. Ik heb er nog een paar extra pagina’s bij gemaakt en ze in Marvelapp gezet zodat het een clickable prototype is, verder heb ik de link in mijn leerdossier vermeld. Ik ben hier tot de middag mee bezig geweest en later op de dag was er een shine moment. Zelf had ik me niet ingeschreven voor dit moment en ben ook daarna naar huis gegaan.
