---
title: maandag 20 november
date: 2017-11-20
---

Vandaag zijn we als team verder gegaan met de debriefing. Ook hebben we team afspraken gemaakt, een swot en nog kwartaal doelen gesteld. Nadat we hier als team mee klaar waren zijn we gaan kijken naar de merkanalyse. Dit was in het begin nog een beetje onduidelijk maar uiteindelijk zijn we er met z’n allen uit gekomen. Ook hebben we  nog de merkwijzer ingevuld om een nog beter beeld van Paard te krijgen.
