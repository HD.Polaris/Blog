---
title: Vrijdag 23 november
date: 2018-11-22
---
Vandaag hebben we een businessmodel canvas gemaakt om de partners en de benodigde middelen centraal te zetten om te kijken of wij ons project kunnen realiseren. Hiervoor heb ik een volledige uitleg gekregen bij mijn lab creative concepting. Dit is een erg nuttige methode omdat je alles kan bekijken wie en wat er bij komt kijken en wat we er voor nodig hebben.
