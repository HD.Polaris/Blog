---
title: Vrijdag 08 maart
date: 2019-03-08
---
Vandaag heb ik de rest van mijn onderzoek afgemaakt en heb ik effectief de behandelde leerstof uit de hoorcolleges toegepast. Verder heb ik nog andere literatuur opgezocht in de mediatheek en thuis. Uiteindelijk ben ik erg tevreden met mijn gedeelte van het onderzoek en wacht ik de feedback af. 
