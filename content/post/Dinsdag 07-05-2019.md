---
title: Dinsdag 07 mei
date: 2019-05-07
---
Vandaag is de eerste nieuwe studio dag, we zijn begonnen met een reflectie van het afgelopen kwartaal. We hebben ons concept even nagetrokken om te kijken waar we ook alweer zijn gebleven. Ook zijn we opzoek gegaan naar nieuwe methodes om meer diepgang in ons concept te vinden, uiteindelijk bleek dit allemaal besproken te zijn in het URT lab. Daarom hebben we de presentaties terug gelezen om zo de methodes uit te zoeken.
