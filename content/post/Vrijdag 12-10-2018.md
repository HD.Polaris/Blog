---
title: Vrijdag 12 oktober
date: 2018-10-12
---
Vandaag zijn we met het team gaan brainstormen voor concepten, we hebben hiervoor allemaal drie onderzoeksmethodes gekozen en hebben toen gekozen welke we gingen doen. We hebben er twee voor divergeren gedaan en twee voor convergeren. Dit waren de Braindump, COCD box, Negatief brainstormen en de stickermethode. We hebben vier rondes braindump gedaan en elke keer geprobeerd om iets nieuws te bedenken bij het concept en daardoor zoveel mogelijk concepten gegenereerd. Hierna hebben we het nog een keer 4x gedaan maar dan met een nieuwe stelling namelijk (Hoe kan je ondernemers werven door de klantenservice te verbeteren) en hier hebben we toen 12 concepten uit gehaald. Dit zijn nog basis ideeën en ze kunnen nog elke kant op.
