---
title: Dondergag 7 September
date: 2017-09-07
---

Vandaag zijn we als team te werk gegaan aan een design theory, het maken van een poster door middel van het ontwerpproces, een presentatie daar over geven, feedback ontvangen en daarop reflecteren en het maken van een swot analyse. 