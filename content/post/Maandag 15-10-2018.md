---
title: Maandag 15 oktober
date: 2018-10-15
---
Vandaag zijn we nog een keer gaan brainstormen voor concepten en hebben hiervoor de COCD box methode gebruikt. Hiervoor hebben we drie rondes gedaan. Daarna zijn we de ideeën gaan uitkammen. We hebben ideeën gecombineerd en aangepast totdat we uiteindelijk op drie concepten uitkwamen. Deze concepten worden nu uitgeschreven tot echte concepten en verwerkt in drie aparte conceptposters. Daarna hebben Janice en ik vragen bedacht voor ons interview met Armando morgen. Hiervoor hebben we vragen bedacht die relevant zijn tot de ontwerpvraag en voor punten waar we vast lopen en dingen die we echt nog moeten/willen weten.
