---
title: Vrijdag 30 november
date: 2018-11-30
---
Vandaag Heb ik mijn geschetste schermen ook digitaal gemaakt met Adobe Illustrator. Ik heb ze allemaal uitgewerkt op een standaard scherm formaat en heb hiervoor ook de iconen gemaakt. Verder heb ik me aan de team styleguide gehouden om ervoor te zorgen dat we allemaal in dezelfde stijl zouden werken en we een samenhang zouden creëren.
